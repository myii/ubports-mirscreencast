#!/bin/sh

# Initial variables
RATIO_SCREEN_WIDTH=9
RATIO_SCREEN_HEIGHT=16
DEFAULT_SCREEN_WIDTH=480
DEFAULT_IP=nexus5
DEFAULT_PORT=12345
DEFAULT_FORMAT=RGBA
# DEFAULT_FORMAT=BGRA
DEFAULT_ORIENTATION=p0
DEFAULT_CAP_INTERVAL=1

# Get user input
read -p"Screen width? [${DEFAULT_SCREEN_WIDTH}] " SCREEN_WIDTH
read -p"Device IP? [${DEFAULT_IP}] " IP
read -p"Port? [${DEFAULT_PORT}] " PORT
read -p"Format? [${DEFAULT_FORMAT}] " FORMAT
read -p"Orientation (p0/l1/l2)? [${DEFAULT_ORIENTATION}] " ORIENTATION
read -p"Capture interval? [${DEFAULT_CAP_INTERVAL}] " CAP_INTERVAL
echo

if [ "${SCREEN_WIDTH}" = "" ] ; then
    SCREEN_WIDTH=${DEFAULT_SCREEN_WIDTH}
fi
SCREEN_HEIGHT=$((${SCREEN_WIDTH} * ${RATIO_SCREEN_HEIGHT} / ${RATIO_SCREEN_WIDTH}))

if [ "${IP}" = "" ] ; then
    IP=${DEFAULT_IP}
fi

if [ "${PORT}" = "" ] ; then
    PORT=${DEFAULT_PORT}
fi

if [ "${FORMAT}" = "" ] ; then
    FORMAT=${DEFAULT_FORMAT}
fi

if [ "${ORIENTATION}" = "" ] ; then
    ORIENTATION=${DEFAULT_ORIENTATION}
fi
ROTATE="-vf rotate="
if [ "${ORIENTATION}" = "l1" ] ; then
    ROTATE=${ROTATE}1
elif [ "${ORIENTATION}" = "l2" ] ; then
    ROTATE=${ROTATE}2
else
    ROTATE=
fi

if [ "${CAP_INTERVAL}" = "" ] ; then
    CAP_INTERVAL=${DEFAULT_CAP_INTERVAL}
fi

LOCAL_COMMAND='nc -l -p ${PORT} | gzip -dc | mplayer -demuxer rawvideo -rawvideo w=${SCREEN_WIDTH}:h=${SCREEN_HEIGHT}:format=${FORMAT} ${ROTATE} -'
REMOTE_COMMAND="LOCAL_IP=\$(echo \${SSH_CLIENT} | cut -d' ' -f1) && mirscreencast -m /run/mir_socket --stdout --cap-interval ${CAP_INTERVAL} -s ${SCREEN_WIDTH} ${SCREEN_HEIGHT} | gzip -c | nc \${LOCAL_IP} ${PORT}"
ssh -f phablet@${IP} "${REMOTE_COMMAND}"
eval ${LOCAL_COMMAND}
